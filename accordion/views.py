from django.shortcuts import render

# Create your views here.
def accordionView(request):
    return render(request, 'accordion.html')
